/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_press_key.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/24 13:14:59 by mkrutik           #+#    #+#             */
/*   Updated: 2017/02/24 16:58:18 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_head.h"

void	ft_minus_centr(t_data *src, int i, int j, int m)
{
	while (i < src->line)
	{
		j = 0;
		while (j < src->n_p)
		{
			src->map[i][j]->x -= m * src->centr_x;
			src->map[i][j]->y -= m * src->centr_y;
			j++;
		}
		i++;
	}
}

void	ft_do(t_data *src, int i, int j)
{
	double	x;
	double	y;

	x = src->centr_x_b - src->centr_x;
	y = src->centr_y_b - src->centr_y;
	while (i < src->line)
	{
		j = 0;
		while (j < src->n_p)
		{
			src->map[i][j]->x += x;
			src->map[i][j]->y += y;
			j++;
		}
		i++;
	}
}

void	ft_key_zoom(t_data *src, int key)
{
	ft_minus_centr(src, 0, 0, 1);
	src->centr_x_b = src->centr_x;
	src->centr_y_b = src->centr_y;
	(key == K_ZOOM_M) ? (ft_zoom(src, ZOOM_M)) : 0;
	(key == K_ZOOM_P) ? (ft_zoom(src, ZOOM_P)) : 0;
	ft_find_centr(src, 0, 0);
	ft_do(src, 0, 0);
	ft_minus_centr(src, 0, 0, -1);
	src->centr_x = src->centr_x_b;
	src->centr_y = src->centr_y_b;
}

void	ft_key_rot(t_data *src, int key)
{
	ft_minus_centr(src, 0, 0, 1);
	src->centr_x_b = src->centr_x;
	src->centr_y_b = src->centr_y;
	(key == ROT_X_M) ? (ft_axes_x(src, RAD_M)) : 0;
	(key == ROT_X_P) ? (ft_axes_x(src, RAD_P)) : 0;
	(key == ROT_Y_M) ? (ft_axes_y(src, RAD_M)) : 0;
	(key == ROT_Y_P) ? (ft_axes_y(src, RAD_P)) : 0;
	(key == ROT_Z_P) ? (ft_axes_z(src, RAD_P)) : 0;
	(key == ROT_Z_M) ? (ft_axes_z(src, RAD_M)) : 0;
	ft_find_centr(src, 0, 0);
	ft_do(src, 0, 0);
	ft_minus_centr(src, 0, 0, -1);
	src->centr_x = src->centr_x_b;
	src->centr_y = src->centr_y_b;
}

int		ft_press_key(int key, t_data *src)
{
	(key == EXIT) ? (exit(0)) : 0;
	if (key == K_ZOOM_P || key == K_ZOOM_M)
		ft_key_zoom(src, key);
	else if (key == SHIFT_U || key == SHIFT_D || key == SHIFT_R ||
			key == SHIFT_L)
	{
		ft_minus_centr(src, 0, 0, 1);
		(key == SHIFT_U) ? (src->centr_y -= 50) : 0;
		(key == SHIFT_D) ? (src->centr_y += 50) : 0;
		(key == SHIFT_L) ? (src->centr_x -= 50) : 0;
		(key == SHIFT_R) ? (src->centr_x += 50) : 0;
		src->centr_x_b = src->centr_x;
		src->centr_y_b = src->centr_y;
		ft_minus_centr(src, 0, 0, -1);
		src->centr_x = src->centr_x_b;
		src->centr_y = src->centr_y_b;
	}
	else if (key == ROT_X_M || key == ROT_X_P || key == ROT_Y_M || key ==
			ROT_Y_P || key == ROT_Z_P || key == ROT_Z_M)
		ft_key_rot(src, key);
	else
		return (0);
	ft_create_img(src);
	return (0);
}

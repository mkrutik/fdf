/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_error.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/24 12:52:49 by mkrutik           #+#    #+#             */
/*   Updated: 2017/02/24 12:53:23 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_head.h"

void	ft_error(int n)
{
	ft_putstr("\033[0;31m");
	write(1, "ERROR\n", 6);
	if (n == 1)
		write(1, "Wrong number of arguments\n", 26);
	else if (n == 2)
		write(1, "File doesn`t open, maybe wrong file name\n", 41);
	else if (n == 3)
		write(1, "It`s not possible to read the file\n", 35);
	else if (n == 4)
		write(1, "It`s not possible to close the file\n", 36);
	else if (n == 5)
		write(1, "MAP file doesn`t valid\n", 23);
	write(1, "USAGE: ./fdf <file name>\n", 25);
	exit(-1);
}

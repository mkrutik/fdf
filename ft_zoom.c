/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_zoom.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/24 13:35:34 by mkrutik           #+#    #+#             */
/*   Updated: 2017/02/24 13:35:43 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_head.h"

void	ft_zoom(t_data *src, double zoom)
{
	int	i;
	int	j;

	i = 0;
	while (i < src->line)
	{
		j = 0;
		while (j < src->n_p)
		{
			src->map[i][j]->x *= zoom;
			src->map[i][j]->y *= zoom;
			src->map[i][j]->z *= zoom;
			j++;
		}
		i++;
	}
}

void	ft_find_centr(t_data *src, int i, int j)
{
	double		max_x;
	double		max_y;
	double		min_y;
	double		min_x;

	max_x = src->map[0][0]->x;
	min_x = src->map[0][0]->x;
	min_y = src->map[0][0]->y;
	max_y = src->map[0][0]->y;
	while (i < src->line)
	{
		j = 0;
		while (j < src->n_p)
		{
			(src->map[i][j]->x > max_x) ? (max_x = src->map[i][j]->x) : 0;
			(src->map[i][j]->x < min_x) ? (min_x = src->map[i][j]->x) : 0;
			(src->map[i][j]->y > max_y) ? (max_y = src->map[i][j]->y) : 0;
			(src->map[i][j]->y < min_y) ? (min_y = src->map[i][j]->y) : 0;
			j++;
		}
		i++;
	}
	src->centr_x = (max_x + min_x) / 2;
	src->centr_y = (max_y + min_y) / 2;
}

void	ft_put_to_centr(t_data *src, int i, int j)
{
	double		shift_x;
	double		shift_y;

	shift_x = src->w_centr_x - src->centr_x;
	shift_y = src->w_centr_y - src->centr_y;
	(src->centr_x < 0) ? (shift_x += (-1 * src->centr_x)) : 0;
	(src->centr_y < 0) ? (shift_y += (-1 * src->centr_y)) : 0;
	while (i < src->line)
	{
		j = 0;
		while (j < src->n_p)
		{
			src->map[i][j]->x += shift_x;
			src->map[i][j]->y += shift_y;
			j++;
		}
		i++;
	}
	src->centr_x = src->w_centr_x;
	src->centr_y = src->w_centr_y;
}

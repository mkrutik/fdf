/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/24 13:37:57 by mkrutik           #+#    #+#             */
/*   Updated: 2017/02/24 17:12:48 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_head.h"

void	ft_wind_from_arg(t_data *src, char **argv)
{
	int		w_x;
	int		w_y;

	w_x = ft_atoi(argv[2]);
	w_y = ft_atoi(argv[3]);
	if (w_x < 10 || w_x > 2000 || w_y < 0 || w_y > 1300)
	{
		src->w_centr_x = W_X / 2;
		src->w_centr_y = W_Y / 2;
	}
	else
	{
		src->w_centr_x = w_x;
		src->w_centr_y = w_y;
	}
}

int		main(int argc, char **argv)
{
	t_data	*src;

	(argc < 2 || argc > 4 || argc == 3) ? (ft_error(1)) : 0;
	src = (t_data*)malloc(sizeof(t_data));
	(argc == 4) ? (ft_wind_from_arg(src, argv)) : (src->w_centr_x = W_X / 2);
	(argc == 4) ? (ft_wind_from_arg(src, argv)) : (src->w_centr_y = W_Y / 2);
	src->line = 0;
	src->n_p = -1;
	src->name = ft_strdup(argv[1]);
	ft_valid(argv[1], &src->line, &src->n_p, 0);
	src->map = (t_point***)malloc(sizeof(t_point**) * src->line);
	ft_create_map(src, 0, 0);
	(src->line < 200) ? (ft_zoom(src, 10)) : 0;
	ft_find_centr(src, 0, 0);
	ft_put_to_centr(src, 0, 0);
	src->mlx = mlx_init();
	src->win = mlx_new_window(src->mlx, (src->w_centr_x * 2),
			(src->w_centr_y * 2), src->name);
	ft_create_img(src);
	mlx_hook(src->win, 2, 5, ft_press_key, src);
	mlx_loop(src->mlx);
	return (0);
}

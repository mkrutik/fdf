# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/02/15 10:42:44 by mkrutik           #+#    #+#              #
#    Updated: 2017/02/24 17:15:08 by mkrutik          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = fdf

SRC =	ft_def_color.c \
		ft_press_key.c \
		ft_zoom.c \
		ft_error.c \
		ft_put_pixel.c \
		get_next_line.c \
		ft_create_img.c \
		ft_find_color.c \
		ft_rotation.c \
		ft_create_map.c \
		ft_valid.c \
		main.c

OBJ = $(SRC:.c=.o)

HEAD = -I ft_head.h get_next_line.h libft/libft.h

GFLAGS = -Wall -Wextra -Werror

LIBINC = -I libft/libft.h -L./libft -lft

FRAMEWORK = -lmlx -framework OpenGL -framework AppKit

all: $(NAME)

$(NAME): $(OBJ)
	make -C libft/
	gcc $(GFLAGS) $(OBJ) -o $(NAME) $(LIBINC) $(FRAMEWORK)

clean:
	rm -f $(OBJ)
	make clean -C libft/

fclean: clean
	rm -f $(NAME)
	make fclean -C libft/

re: fclean all

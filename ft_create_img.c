/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_create_img.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/24 12:37:29 by mkrutik           #+#    #+#             */
/*   Updated: 2017/02/24 16:51:18 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_head.h"

void	ft_put_man(t_data *src)
{
	mlx_string_put(src->mlx, src->win, 15, 1020, 4454836, "[ESC]");
	mlx_string_put(src->mlx, src->win, 15, 1040, 4627967,
			"[+]			Zoom plus");
	mlx_string_put(src->mlx, src->win, 15, 1060, 4627967,
			"[-]			Zoom minus");
	mlx_string_put(src->mlx, src->win, 15, 1080, 4514791,
			"[arrow right]  Shift right");
	mlx_string_put(src->mlx, src->win, 15, 1100, 4514791,
			"[arrow left]   Shift left");
	mlx_string_put(src->mlx, src->win, 15, 1120, 4514791,
			"[arrow up]	 Shift up");
	mlx_string_put(src->mlx, src->win, 15, 1140, 4514791,
			"[arrow down]   Shift down");
	mlx_string_put(src->mlx, src->win, 15, 1160, 16366592,
			"[4]			Rotation left");
	mlx_string_put(src->mlx, src->win, 15, 1180, 16366592,
			"[6]			Rotation right");
	mlx_string_put(src->mlx, src->win, 15, 1200, 6420113,
			"[8]			Rotation up");
	mlx_string_put(src->mlx, src->win, 15, 1220, 6420113,
			"[2]			Rotation down");
	mlx_string_put(src->mlx, src->win, 15, 1240, 16711680,
			"[1]			Rotation left by z");
	mlx_string_put(src->mlx, src->win, 15, 1260, 16711680,
			"[3]			Rotation right by z");
}

void	ft_create_img(t_data *src)
{
	t_point	*p1;

	p1 = (t_point*)malloc(sizeof(t_point));
	src->img = mlx_new_image(src->mlx, (src->w_centr_x * 2),
			(src->w_centr_y * 2));
	src->img_data = mlx_get_data_addr(src->img, &src->pixel,
			&src->len, &src->end);
	ft_line(src, -1, -1, p1);
	free(p1);
	mlx_put_image_to_window(src->mlx, src->win, src->img, 0, 0);
	ft_put_man(src);
	mlx_destroy_image(src->mlx, src->img);
}

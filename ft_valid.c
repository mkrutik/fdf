/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_valid.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/24 13:33:51 by mkrutik           #+#    #+#             */
/*   Updated: 2017/02/24 13:33:58 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_head.h"

int		ft_if_color(char *src, int i)
{
	while (ft_isdigit(src[i]))
		i++;
	while (ft_isdigit(src[i]) || src[i] == ',' || src[i] == 'x' || (src[i] >= 65
				&& src[i] <= 70) || (src[i] >= 97 && src[i] <= 102))
		i++;
	return (i);
}

void	ft_open_read_close(char **res, char *name)
{
	char	*tmp;
	int		i;
	int		fd;

	if ((fd = open(name, O_RDONLY)) < 0)
		ft_error(2);
	tmp = (char*)malloc(sizeof(char) * 100000001);
	if ((i = read(fd, tmp, 100000000)) < 0)
		ft_error(3);
	if (close(fd) < 0)
		ft_error(4);
	tmp[i] = '\0';
	*res = ft_strdup(tmp);
	ft_strdel(&tmp);
}

void	ft_valid(char *name, int *l, int *n_p, int i)
{
	char	*res;
	int		point;

	ft_open_read_close(&res, name);
	point = 0;
	while (res[i])
	{
		(res[i] == ' ') ? (i++) : 0;
		if (res[i] == '\n')
		{
			i++;
			(*l)++;
			(*n_p == -1) ? (*n_p = point) : 0;
			(point < *n_p) ? (ft_error(5)) : 0;
			point = 0;
		}
		if (ft_isdigit(res[i]) || (ft_isdigit(res[i + 1]) && res[i] == '-'))
		{
			point++;
			i = ft_if_color(res, ++i);
		}
		else if (res[i] != ' ' && res[i] != '\n' && res[i] != '\0')
			i++;
	}
	(res[--i] != '\n') ? ((*l)++) : 0;
}

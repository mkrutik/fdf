/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_create_map.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/24 12:47:59 by mkrutik           #+#    #+#             */
/*   Updated: 2017/02/24 12:48:07 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_head.h"

void	ft_create_map(t_data *src, int l_index, int line)
{
	char	*tmp;
	char	*p;

	src->fd = open(src->name, O_RDONLY);
	tmp = NULL;
	while (line < src->line)
	{
		src->map[line] = (t_point**)malloc(sizeof(t_point*) * src->n_p);
		get_next_line(src->fd, &tmp);
		p = tmp;
		l_index = 0;
		while (l_index < src->n_p)
		{
			src->map[line][l_index] = (t_point*)malloc(sizeof(t_point));
			src->map[line][l_index]->x = l_index;
			src->map[line][l_index]->y = line;
			tmp += ft_find_z_and_color(&src->map[line][l_index]->z, tmp,
					src->map[line][l_index]);
			l_index++;
		}
		ft_strdel(&p);
		line++;
	}
	if (close(src->fd) < 0)
		ft_error(4);
}

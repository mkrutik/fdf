/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_put_pixel.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/24 13:21:26 by mkrutik           #+#    #+#             */
/*   Updated: 2017/02/24 17:10:57 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_head.h"

void	ft_put_pixel(t_point *elem, t_data *src)
{
	int	point;

	if (elem->x >= 0 && elem->x < (src->w_centr_x * 2) && elem->y >= 0
			&& elem->y < (src->w_centr_y * 2))
	{
		point = elem->x * (src->pixel / 8) + elem->y * src->len;
		if (src->end == 0)
		{
			src->img_data[point++] = elem->b;
			src->img_data[point++] = elem->g;
			src->img_data[point] = elem->r;
		}
		else
		{
			src->img_data[point++] = elem->r;
			src->img_data[point++] = elem->g;
			src->img_data[point] = elem->b;
		}
	}
}

void	do_line(t_point *p1, t_point *p2, t_data *src, int error2)
{
	int		delta_x;
	int		delta_y;
	int		sign_x;
	int		sign_y;
	int		error;

	delta_x = fabs(p2->x - p1->x);
	delta_y = fabs(p2->y - p1->y);
	sign_x = p1->x < p2->x ? 1 : -1;
	sign_y = p1->y < p2->y ? 1 : -1;
	error = delta_x - delta_y;
	ft_put_pixel(p2, src);
	while (p1->x != p2->x || p1->y != p2->y)
	{
		ft_put_pixel(p1, src);
		error2 = error * 2;
		(error2 > -delta_y) ? (error -= delta_y) : 0;
		(error2 > -delta_y) ? (p1->x += sign_x) : 0;
		if (error2 < delta_x)
		{
			error += delta_x;
			p1->y += sign_y;
		}
	}
}

void	ft_copy_point(t_point *src, t_point *tmp, int f)
{
	tmp->r = src->r;
	tmp->g = src->g;
	tmp->b = src->b;
	if (f == 0)
	{
		tmp->x = src->x;
		tmp->y = src->y;
		tmp->z = src->z;
	}
	else
	{
		tmp->x = (int)src->x;
		tmp->y = (int)src->y;
		tmp->z = (int)src->z;
	}
}

void	ft_line(t_data *src, int i, int j, t_point *p1)
{
	t_point	*p2;

	p2 = (t_point*)malloc(sizeof(t_point));
	while (++i < src->line)
	{
		j = -1;
		while (++j < src->n_p)
		{
			if ((j + 1) < src->n_p)
			{
				ft_copy_point(src->map[i][j + 1], p2, 1);
				ft_copy_point(src->map[i][j], p1, 1);
				do_line(p1, p2, src, 0);
			}
			if ((i + 1) < src->line)
			{
				ft_copy_point(src->map[i][j], p1, 1);
				ft_copy_point(src->map[i + 1][j], p2, 1);
				do_line(p1, p2, src, 0);
			}
		}
	}
	free(p2);
}

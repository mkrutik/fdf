/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_find_color.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/24 13:07:12 by mkrutik           #+#    #+#             */
/*   Updated: 2017/02/24 13:07:53 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_head.h"

int		ft_valid_color(char c)
{
	if (ft_isdigit(c) || (c >= 65 && c <= 70) || (c >= 97 && c <= 102))
		return (1);
	else
		return (0);
}

int		ft_atoi_hex(char *src)
{
	int		res;
	int		i;

	i = 0;
	res = 0;
	while (src[i])
	{
		if (ft_isdigit(src[i]))
			res = res * 16 + (src[i] - '0');
		else
		{
			ft_toupper(src[i]);
			res = res * 16 + (src[i] - 'A' + 10);
		}
		i++;
	}
	return (res);
}

void	ft_save_color(char *tmp, t_point *map)
{
	char	*r;
	char	*g;
	char	*b;

	r = ft_strsub(tmp, 0, 2);
	g = ft_strsub(tmp, 2, 2);
	b = ft_strsub(tmp, 4, 2);
	map->r = ft_atoi_hex(r);
	map->g = ft_atoi_hex(g);
	map->b = ft_atoi_hex(b);
	ft_strdel(&r);
	ft_strdel(&g);
	ft_strdel(&b);
	ft_strdel(&tmp);
}

void	ft_find_color(char *tmp, int end, t_point *map, int v)
{
	char	color[7];
	char	*res;

	while (tmp[end] != '\0' && tmp[end] != '\n' && tmp[end] != ' ')
	{
		if (ft_valid_color(tmp[end]))
		{
			color[v] = tmp[end];
			v++;
		}
		else
			break ;
		end++;
	}
	color[v] = '\0';
	res = NULL;
	(v == 0) ? (res = ft_strdup("000000")) : 0;
	(v == 1) ? (res = ft_strjoin("00000", color)) : 0;
	(v == 2) ? (res = ft_strjoin("0000", color)) : 0;
	(v == 3) ? (res = ft_strjoin("000", color)) : 0;
	(v == 4) ? (res = ft_strjoin("00", color)) : 0;
	(v == 5) ? (res = ft_strjoin("0", color)) : 0;
	(v == 6) ? (res = ft_strdup(color)) : 0;
	ft_save_color(res, map);
}

int		ft_find_z_and_color(double *z, char *tmp, t_point *map)
{
	int		start;
	int		end;

	start = 0;
	while (tmp[start] != '-' && !ft_isdigit(tmp[start]))
		start++;
	end = start;
	while (tmp[end] != ' ' && tmp[end] != ',' && tmp[end] != '\0'
			&& tmp[end] != '\n')
		end++;
	*z = ft_atoi(ft_strsub(tmp, start, (end - start)));
	if (tmp[end] == ',' && tmp[end + 1] == '0' && tmp[end + 2] == 'x')
		ft_find_color(tmp, end + 3, map, 0);
	else
		ft_def_col(map);
	if (tmp[end] == ',')
		while (tmp[end] != '\0' && tmp[end] != '\n' && tmp[end] != ' ')
			end++;
	return (end);
}

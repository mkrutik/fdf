/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rotation.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/24 13:31:56 by mkrutik           #+#    #+#             */
/*   Updated: 2017/02/24 13:32:03 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_head.h"

void	ft_axes_x(t_data *src, double rad)
{
	int		i;
	int		j;
	t_point	*tmp;

	i = 0;
	tmp = (t_point*)malloc(sizeof(t_point));
	while (i < src->line)
	{
		j = 0;
		while (j < src->n_p)
		{
			ft_copy_point(src->map[i][j], tmp, 0);
			src->map[i][j]->y = tmp->y * cos(rad) - tmp->z * sin(rad);
			src->map[i][j]->z = tmp->y * sin(rad) + tmp->z * cos(rad);
			j++;
		}
		i++;
	}
}

void	ft_axes_y(t_data *src, double rad)
{
	int		i;
	int		j;
	t_point	*tmp;

	i = 0;
	tmp = (t_point*)malloc(sizeof(t_point));
	while (i < src->line)
	{
		j = 0;
		while (j < src->n_p)
		{
			ft_copy_point(src->map[i][j], tmp, 0);
			src->map[i][j]->x = tmp->x * cos(rad) - tmp->z * sin(rad);
			src->map[i][j]->z = tmp->x * sin(rad) + tmp->z * cos(rad);
			j++;
		}
		i++;
	}
}

void	ft_axes_z(t_data *src, double rad)
{
	int		i;
	int		j;
	t_point	*tmp;

	i = 0;
	tmp = (t_point*)malloc(sizeof(t_point));
	while (i < src->line)
	{
		j = 0;
		while (j < src->n_p)
		{
			ft_copy_point(src->map[i][j], tmp, 0);
			src->map[i][j]->x = tmp->x * cos(rad) + tmp->y * sin(rad);
			src->map[i][j]->y = -tmp->x * sin(rad) + tmp->y * cos(rad);
			j++;
		}
		i++;
	}
}

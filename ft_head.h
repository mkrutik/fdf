/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_head.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/24 12:32:14 by mkrutik           #+#    #+#             */
/*   Updated: 2017/02/24 17:15:18 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_HEAD_H
# define FT_HEAD_H
# include "libft/libft.h"
# include "get_next_line.h"
# include <mlx.h>
# include <math.h>

# define EXIT 53
# define K_ZOOM_P 69
# define K_ZOOM_M 78
# define SHIFT_R 124
# define SHIFT_L 123
# define SHIFT_U 126
# define SHIFT_D 125
# define ROT_X_P 91
# define ROT_X_M 84
# define ROT_Y_P 86
# define ROT_Y_M 88
# define ROT_Z_P 83
# define ROT_Z_M 85

# define ZOOM_P 1.5
# define ZOOM_M 0.5

# define RAD_P M_PI/72
# define RAD_M -M_PI/72

# define W_X 2000
# define W_Y 1300

typedef struct		s_point
{
	double			x;
	double			y;
	double			z;
	int				r;
	int				g;
	int				b;
}					t_point;

typedef struct		s_data
{
	t_point			***map;
	int				line;
	int				n_p;
	char			*name;
	int				fd;
	double			centr_x;
	double			centr_y;
	double			centr_x_b;
	double			centr_y_b;
	int				w_centr_x;
	int				w_centr_y;
	void			*mlx;
	void			*win;
	void			*img;
	char			*img_data;
	int				len;
	int				pixel;
	int				end;
}					t_data;

void				ft_error(int n);
void				ft_valid(char *name, int *line, int *n_p, int i);
void				ft_create_map(t_data *src, int index, int line);
void				ft_put_pixel(t_point *elem, t_data *src);
void				do_line(t_point *p1, t_point *p2, t_data *src, int i);
void				ft_zoom(t_data *src, double zoom);
void				ft_line(t_data *src, int i, int j, t_point *p1);
void				ft_axes_x(t_data *src, double rad);
void				ft_axes_y(t_data *src, double rad);
void				ft_axes_z(t_data *src, double rad);
void				ft_copy_point(t_point *src, t_point *tmp, int f);
void				ft_find_centr(t_data *src, int i, int j);
void				ft_put_to_centr(t_data *src, int i, int j);
void				ft_create_img(t_data *src);
int					ft_press_key(int key, t_data *src);
void				ft_minus_centr(t_data *src, int i, int j, int m);
int					ft_find_z_and_color(double *z, char *tmp, t_point *map);
void				ft_def_col(t_point *elem);
#endif
